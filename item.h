#include "world.h"

#ifndef ITEM_H
#define ITEM_H

#include "entity.h"

class Item : public Entity
{
public:
    Item(World &world, std::string script);
};

#endif // ITEM_H
