#ifndef WORLD_H
#define WORLD_H

#include <algorithm>
#include <memory>
#include <vector>

#include "Box2D/Box2D.h"
#include "creature.h"
#include "entity.h"
#include "item.h"

using ms = std::chrono::milliseconds;

class Entity;

class World : public b2World
{
public:
    World();

    void add_entity(std::shared_ptr<Entity> entity);
    void delete_entity(unsigned id, std::string type);
    unsigned get_entity_index(unsigned id);
    std::vector<std::shared_ptr<Creature>> & get_creatures();
    void update_entities();

    void update();

    unsigned new_entity_id();
    ms get_current_time();

    void set_packet(sf::Packet *packet);
    sf::Packet * get_packet();

protected:
    std::vector<std::shared_ptr<Creature>> creatures;
    std::vector<std::shared_ptr<Item>> items;
    unsigned entity_id;

    ms current_time;

    sf::Packet *packet;
};

#endif // WORLD_H
