#ifndef CLIENT_H
#define CLIENT_H

#include "server.h"
#include "creature.h"

class Server;

class Client
{
public:
    Client(Server &server);

    void handle_packet();
    void receive();
    void send(sf::Packet &packet);

    sf::TcpSocket & get_socket();

protected:
    Server &server;
    sf::TcpSocket socket;
    sf::Packet packet;
    unsigned id;

    std::weak_ptr<Creature> creature;
};

#endif // CLIENT_H
