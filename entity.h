#include "world.h"

#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Network.hpp>
#include <iostream>
#include <Python.h>
#include <cstring>

#include "Box2D/Box2D.h"
#include "misc.h"

class World;

class Entity
{
public:
    Entity(World &world, std::string script);
    ~Entity();

    b2Body * get_body();
    unsigned get_id();
    std::string get_type();

    sf::Packet & operator <<(sf::Packet &packet, Entity &entity);

protected:
    b2Body *body;
    unsigned id;
    std::string type;
    World &world;
};

#endif // ENTITY_H
