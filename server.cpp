#include "server.h"

Server::Server()
{
    if (this->listener.listen(54000) != sf::Socket::Done)
    {
        std::cout << "cannot bind socket" << std::endl;
    }

    this->selector.add(this->listener);

    world.set_packet(&this->packet);
}



void Server::update()
{
    while (true)
    {
        this->world.update();
        this->accept_new_connection();
        this->handle_clients();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}



void Server::accept_new_connection()
{
    if (this->selector.isReady(this->listener))
    {
        auto new_client = std::unique_ptr<Client>(new Client(*this));

        if (listener.accept(new_client->get_socket()) == sf::TcpSocket::Done)
        {
            this->selector.add(new_client->get_socket());
            this->clients.push_back(std::move(new_client));
        }
        else
        {
            std::cout << "cannot accept new connection" << std::endl;
        }
    }
}



void Server::handle_clients()
{
    for (auto &it: this->clients)
    {
        it->send(this->packet);
        it->receive();
        it->handle_packet();
    }
}



unsigned Server::new_client_id()
{
    return this->client_id;
}
