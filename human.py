
size_x = 1
density = 60
restitution = 0.1
angular_damping = 0.1
linear_damping = 0.1
friction = 0.1
shape_type = "c"
body_type = "d"
bullet = 0

agility = 1
strength = 1
vitality = 1

base_attack = 10
base_attack_speed = 2
base_move_speed = 3.0


