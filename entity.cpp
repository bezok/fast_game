#include "entity.h"

Entity::Entity(World &world, std::string script)
    : world(world)
{
    this->id = world.new_entity_id();

    PyObject *module;
    b2BodyDef body_def;
    b2FixtureDef fixture_def;
    b2Shape *shape;

    auto packet = this->world.get_packet();

    module = PyImport_ImportModule(script.c_str());

    body_def.angularDamping = get_float_attribute(module, "angular_damping");
    body_def.linearDamping = get_float_attribute(module, "linear_damping");
    body_def.bullet = get_float_attribute(module, "bullet");
    body_def.userData = this;

    *packet << body_def.angularDamping;
    *packet << body_def.angularVelocity;
    *packet << body_def.bullet;
    *packet << body_def.linearDamping;
    *packet << body_def.linearVelocity.x;
    *packet << body_def.linearVelocity.y;
    *packet << body_def.position.x;
    *packet << body_def.position.y;

    auto body_type = get_string_attribute(module, "body_type");
    if (std::strcmp(body_type, "d"))
    {
        body_def.type = b2_dynamicBody;
        *packet << 2;
    }
    else if (std::strcmp(body_type, "s"))
    {
        body_def.type = b2_staticBody;
        *packet << 0;
    }
    else
    {
        body_def.type = b2_kinematicBody;
        *packet << 3;
    }

    this->body = world.CreateBody(&body_def);

    fixture_def.density = get_float_attribute(module, "density");
    fixture_def.friction = get_float_attribute(module, "friction");
    fixture_def.restitution = get_float_attribute(module, "restitution");

    *packet << fixture_def.density;
    *packet << fixture_def.friction;
    *packet << fixture_def.restitution;

    auto shape_type = get_string_attribute(module, "shape_type");
    if (std::strcmp(shape_type, "c"))
    {
        auto new_shape = new b2CircleShape();
        new_shape->m_radius = get_float_attribute(module, "size_x");
        shape = new_shape;
        fixture_def.shape = shape;

        *packet << sf::String("c");
        *packet << new_shape->m_radius;
    }
    else if (std::strcmp(shape_type, "p"))
    {
        auto new_shape = new b2PolygonShape();
        auto x = get_float_attribute(module, "size_x");
        auto y = get_float_attribute(module, "size_y");
        new_shape->SetAsBox(x, y);
        shape = new_shape;
        fixture_def.shape = shape;

        *packet << sf::String("p");
        *packet << x;
        *packet << y;
    }

    this->body->CreateFixture(&fixture_def);

    delete shape;
}



Entity::~Entity()
{
    this->world.DestroyBody(this->body);
    auto packet = this->world.get_packet();
    *packet << sf::String("de");
    *packet << this->id;
}



b2Body * Entity::get_body()
{
    return this->body;
}



unsigned Entity::get_id()
{
    return this->id;
}



std::string Entity::get_type()
{
    return this->type;
}



sf::Packet & Entity::operator <<(sf::Packet &packet, Entity &entity)
{
    b2Body *body = entity.get_body();

    packet << sf::String("ue");
    packet << entity.get_id();
    packet << body->GetPosition().x;
    packet << body->GetPosition().y;
    packet << body->GetAngle();
    packet << body->GetLinearVelocity().x;
    packet << body->GetLinearVelocity().y;

    return packet;
}

