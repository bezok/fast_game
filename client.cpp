#include "client.h"

Client::Client(Server &server)
    : server(server)
{
    this->id = server.new_client_id();  
}



void Client::handle_packet()
{

}



void Client::receive()
{
    if (this->server.selector.isReady(this->socket))
    {
        this->socket.receive(this->packet);
    }
}



void Client::send(sf::Packet &packet)
{
    this->socket.send(packet);
}



sf::TcpSocket & Client::get_socket()
{
    return this->socket;
}
