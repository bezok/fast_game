#include "entity.h"

#ifndef CREATURE_H
#define CREATURE_H

#include <iostream>
#include <chrono>

using ms = std::chrono::milliseconds;

class World;

class Creature : public Entity
{
public:
    Creature(World &world, std::string script);

    void attack();
    void hit(float damage);
    void move();

    void set_attack(unsigned id);
    void set_move(float x, float y);

protected:
    float agility;
    float strength;
    float vitality;
    float base_attack;
    float base_move_speed;

    float hp;

    float attribute_ratio;

    unsigned attack_id;
    float x, y;

    ms attack_time;
    ms move_time;
    ms base_attack_speed;
};

#endif // CREATURE_H
