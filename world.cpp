#include "world.h"

World::World()
    : b2World(b2Vec2(0.0f, 0.0f))
{

}



void World::add_entity(std::shared_ptr<Entity> entity)
{
    if (entity->get_type() == "creature")
    {
        this->creatures.push_back(std::static_pointer_cast<Creature>(entity));
    }

    if (entity->get_type() == "Item")
    {
        this->items.push_back(std::static_pointer_cast<Item>(entity));
    }
}



void World::delete_entity(unsigned id, std::string type)
{
    auto index = this->get_entity_index(id);

    if (type == "creature")
    {
        this->creatures.erase(this->creatures.begin() + index);
    }
    else if (type == "item")
    {
        this->items.erase(this->items.begin() + index);
    }
}



unsigned World::get_entity_index(unsigned id)
{
    auto it = find_if(this->creatures.begin(), this->creatures.end(), [&id](const std::shared_ptr<Creature> &obj){return obj->get_id() == id;});

    int index;

    if (it != this->creatures.end())
    {
        index = std::distance(this->creatures.begin(), it);
    }
    else
    {
        auto it = find_if(this->items.begin(), this->items.end(), [&id](const std::shared_ptr<Item> &obj){return obj->get_id() == id;});

        index = std::distance(this->items.begin(), it);
    }

    return index;
}



std::vector<std::shared_ptr<Creature>> & World::get_creatures()
{
    return this->creatures;
}



void World::update_entities()
{
    for (auto it: this->creatures)
    {
        this->packet << it.get();
    }
}



void World::update()
{
    this->Step(1.0/60.0, 8, 3);
}



unsigned World::new_entity_id()
{
    this->entity_id++;
    return this->entity_id;
}



ms World::get_current_time()
{
    return this->current_time;
}



void World::set_packet(sf::Packet *packet)
{
    this->packet = packet;
}



sf::Packet * World::get_packet()
{
    return this->packet;
}
