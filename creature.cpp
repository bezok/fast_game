#include "creature.h"

Creature::Creature(World &world, std::string script)
    : Entity(world, script)
{
    PyObject *module, *settings_module;

    module = PyImport_ImportModule(script.c_str());
    settings_module = PyImport_ImportModule("settings");

    this->attribute_ratio = get_float_attribute(settings_module, "ratio");

    this->agility = get_float_attribute(module, "agility");
    this->strength = get_float_attribute(module, "strength");
    this->vitality = get_float_attribute(module, "vitality");
    this->base_attack = get_float_attribute(module, "base_attack");
    this->base_attack_speed = ms(int(get_float_attribute(module, "base_attack_speed")));
    this->base_move_speed = get_float_attribute(module, "base_move_speed");

    this->hp = this->vitality * this->attribute_ratio;
}



void Creature::attack()
{
    float damage = this->base_attack + (this->strength * this->attribute_ratio);
    auto creature = this->world.get_entity_index(this->attack_id);
    this->world.get_creatures()[creature]->hit(damage);
}



void Creature::hit(float damage)
{
    this->hp -= damage;
}



void Creature::move()
{
    b2Vec2 position = this->body->GetPosition();
    position.x -= this->x;
    position.y -= this->y;

    if (abs(position.x) > 0.1 or abs(position.y) > 0.1)
    {
        b2Vec2 force;
        b2Vec2 velocity = this->body->GetLinearVelocity();
        float mass = this->body->GetMass();
        force.x = mass * abs(velocity.x * position.x);
        force.y = mass * abs(velocity.y * position.y);

        if (force.x > this->agility * this->attribute_ratio)
        {
            force.x = this->agility * this->attribute_ratio;
            if (position.x < 0)
            {
                force.x *= -1;
            }
        }

        if (force.y > this->agility * this->attribute_ratio)
        {
            force.y = this->agility * this->attribute_ratio;
            if (position.y < 0)
            {
                force.y *= -1;
            }
        }

        this->body->ApplyForceToCenter(force, true);
    }
}



void Creature::set_attack(unsigned id)
{
    this->attack_id = id;
}



void Creature::set_move(float x, float y)
{
    this->x = x;
    this->y = y;
}
