#ifndef SERVER_H
#define SERVER_H

#include <SFML/Network.hpp>
#include "world.h"
#include "client.h"
#include <chrono>
#include <thread>
#include <memory>

class Client;

class Server
{
public:
    Server();

    void update();

    void accept_new_connection();
    void handle_clients();

    unsigned new_client_id();

    friend class Client;

protected:
    World world;

    sf::TcpListener listener;
    sf::SocketSelector selector;

    std::vector<std::unique_ptr<Client>> clients;
    sf::Packet packet;

    unsigned client_id;
};

#endif // SERVER_H
