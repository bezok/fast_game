#include <iostream>
#include "server.h"
#include <Python.h>

using namespace std;

int main()
{
    Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append(\".\")");
    PyRun_SimpleString("sys.dont_write_bytecode = True");

    Server server{};
    server.update();

    Py_Finalize();
}
